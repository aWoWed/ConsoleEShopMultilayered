﻿namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public enum Permissions
    {
        ViewProduct,
        SearchProductsByName,
        ReceiveOrderStatus,
        CreateOrder,
        PlaceOrder,
        CancelOrder,
        ChangeOwnInfo,
        Logout,
        ShowProductsByUser,
        ChangeUsers,
        CreateProduct,
        ChangeProduct,
        ChangeOrderStatus
    }
}
