﻿using ConsoleEShopMultilayered.BusinessLogic;
using ConsoleEShopMultilayered.DataAccess.Models;
using ConsoleEShopMultilayered.DataAccess.Repositories;
using ConsoleEShopMultilayered.UserInterface.Controllers;
using ConsoleEShopMultilayered.UserInterface.View;

namespace ConsoleEShopMultilayered.CLI
{
    class Program
    {
        public static void Main(string[] args)
        {
            var console = new ConsoleView();

            var appContext = new AppContext();
            var serviceManager = new ServiceManager();
            var dataManager = new DataManager(appContext, serviceManager);

            var controller = new MainController(console, dataManager);

            controller.Start();
        }
    }
}
