﻿using ConsoleEShopMultilayered.DataAccess.Models;

namespace ConsoleEShopMultilayered.DataAccess.Repositories.Abstract
{
    public interface IProductsRepository : IRepository<Product>
    {
        Product GetProductByName(string name);
        Product GetProductByNameCategoryDescriptionPrice(string name, string category, string description, decimal price);
    }
}
