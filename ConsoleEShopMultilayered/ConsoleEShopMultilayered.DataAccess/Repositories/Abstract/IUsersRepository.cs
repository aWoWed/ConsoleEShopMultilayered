﻿using ConsoleEShopMultilayered.DataAccess.Models;

namespace ConsoleEShopMultilayered.DataAccess.Repositories.Abstract
{
    public interface IUsersRepository : IRepository<User>
    {
        User GetUserByLogin(string login);
        User GetUserByLoginAndPassword(string login, string password);
    }
}
