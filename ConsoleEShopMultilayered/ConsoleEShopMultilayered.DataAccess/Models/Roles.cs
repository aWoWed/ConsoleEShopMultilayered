﻿namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public enum Roles
    {
        Guest,
        User,
        Admin
    }
}
