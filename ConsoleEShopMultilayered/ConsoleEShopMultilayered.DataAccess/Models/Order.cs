﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;

namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public class Order
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("products_ids")]
        public IEnumerable<string> ProductsIds { get; set; }

        [JsonProperty("date_ordered")]
        public DateTime DateOrdered { get; }

        [JsonProperty("order_status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrderStatus OrderStatus { get; set; }

        public Order(IEnumerable<string>? items = null, int? id = null, int? userId = null)
        {
            DateOrdered = DateTime.Now;
            OrderStatus = OrderStatus.New;

            ProductsIds = items ?? new List<string>();

            Id = id ?? 1;
            UserId = userId ?? 1;
        }

        public override string ToString() =>
            $"ID: {Id}\nUser ID: {UserId}\nDate Ordered: {DateOrdered:hh:mm:ss dd-MM-yyyy}\nOrder Status: {OrderStatus}\nProducts:\n{string.Join("\n", ProductsIds)}";
    }
}
