﻿using ConsoleEShopMultilayered.BusinessLogic.Services.Abstract;
using ConsoleEShopMultilayered.BusinessLogic.Services.Entities;

namespace ConsoleEShopMultilayered.BusinessLogic
{
    public class ServiceManager
    {
        public IFileService FileService { get; }
        public IJsonService JsonService { get; }

        public ServiceManager()
        {
            FileService = new FileService();
            JsonService = new JsonService();
        }
    }
}
