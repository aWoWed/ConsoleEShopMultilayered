﻿using Newtonsoft.Json;

namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("role")]
        public Roles Role { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        public User(int? id = null, Roles? role = null, string? login = null, string? password = null)
        {
            Id = id ?? 1;
            Role = role ?? Roles.Guest;
            Login = login ?? "Default Login";
            Password = password ?? "Default Password";
        }

        public override string ToString() =>
            $"ID: {Id}\nRole: {Role}\nLogin: {Login}\nPassword: {Password}";
    }
}
