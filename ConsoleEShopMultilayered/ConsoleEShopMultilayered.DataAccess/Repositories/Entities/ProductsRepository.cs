﻿using System.Collections.Generic;
using System.Linq;
using ConsoleEShopMultilayered.BusinessLogic;
using ConsoleEShopMultilayered.BusinessLogic.Services.Abstract;
using ConsoleEShopMultilayered.BusinessLogic.Services.Entities;
using ConsoleEShopMultilayered.DataAccess.Models;
using ConsoleEShopMultilayered.DataAccess.Repositories.Abstract;

namespace ConsoleEShopMultilayered.DataAccess.Repositories.Entities
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly IDictionary<int, Product> _products;
        private readonly IFileService _fileService;
        private readonly IJsonService _jsonService;

        public ProductsRepository(AppContext context, ServiceManager serviceManager)
        {
            _fileService = serviceManager.FileService;
            _jsonService = serviceManager.JsonService;
            _products = context.Products;
        }

        public void Load(string fileName)
        {
            var products = _jsonService.Deserialize<IEnumerable<Product>>(_fileService.Read(fileName)) ?? new List<Product>();

            foreach (var product in products)
                Save(product);
        }

        public void Write(string fileName) => FileService.Write(fileName, _jsonService.Serialize(_products.Values));

        public IDictionary<int, Product> GetAll() => _products;
        public Product GetById(int id)
        {
            var product = _products[id];
            if (product == null) throw new KeyNotFoundException("Product with such Id does not exist.");
            return product;
        }
        public Product GetProductByName(string name) => _products.Values.FirstOrDefault(prod => prod.Name == name);

        public Product
            GetProductByNameCategoryDescriptionPrice(string name, string category, string description, decimal price) =>
            _products.Values.FirstOrDefault(prod =>
                prod.Name == name && prod.Category == category && prod.Description == description &&
                prod.Price == price);

        public void Save(Product product, bool isWrite = true)
        {

            if (_products.ContainsKey(product.Id))
            {
                _products[product.Id] = product;
                if (isWrite) Write("products.json");
                return;
            }

            _products.Add(product.Id, product);
            if (isWrite) Write("products.json");
        }
    }
}
