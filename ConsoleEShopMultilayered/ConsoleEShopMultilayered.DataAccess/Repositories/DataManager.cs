﻿using ConsoleEShopMultilayered.BusinessLogic;
using ConsoleEShopMultilayered.DataAccess.Models;
using ConsoleEShopMultilayered.DataAccess.Repositories.Abstract;
using ConsoleEShopMultilayered.DataAccess.Repositories.Entities;

namespace ConsoleEShopMultilayered.DataAccess.Repositories
{
    public class DataManager
    {
        public IUsersRepository Users { get; }
        public IProductsRepository Products { get; }
        public IOrdersRepository Orders { get; }

        public DataManager(AppContext context, ServiceManager serviceManager)
        {
            Users = new UsersRepository(context, serviceManager);
            Products = new ProductsRepository(context, serviceManager);
            Orders = new OrdersRepository(context, serviceManager);
        }
    }
}
