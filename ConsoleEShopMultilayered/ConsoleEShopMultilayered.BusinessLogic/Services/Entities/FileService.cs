﻿using System;
using System.IO;
using ConsoleEShopMultilayered.BusinessLogic.Services.Abstract;

namespace ConsoleEShopMultilayered.BusinessLogic.Services.Entities
{
    public class FileService : IFileService
    {
        public static string Read(string fileName)
        {
            if (fileName == null)
                throw new ArgumentNullException($"{nameof(fileName)} cannot be null");

            using var fileStream = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Read);
            using var reader = new StreamReader(fileStream);
            return reader.ReadToEnd();
        }

        public static void Write(string fileName, string text)
        {
            using var writer = new StreamWriter(fileName);
            writer.Write(text);
        }

        string IFileService.Read(string fileName) => Read(fileName);
        void IFileService.Write(string fileName, string text) => Write(fileName, text);
    }
}
