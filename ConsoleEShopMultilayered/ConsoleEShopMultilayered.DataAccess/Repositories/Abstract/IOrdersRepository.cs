﻿using ConsoleEShopMultilayered.DataAccess.Models;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DataAccess.Repositories.Abstract
{
    public interface IOrdersRepository : IRepository<Order>
    {
        Order GetOrderByUserId(int userId);
        List<Order> GetOrdersByUserId(int userId);
    }
}
