﻿namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        ReceivedPayment,
        Sent,
        Received,
        Done
    }
}
