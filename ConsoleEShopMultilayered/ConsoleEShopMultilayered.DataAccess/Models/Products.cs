﻿using Newtonsoft.Json;

namespace ConsoleEShopMultilayered.DataAccess.Models
{
    public class Product
    {

        [JsonProperty("id")] public int Id { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("category")] public string Category { get; set; }

        [JsonProperty("description")] public string Description { get; set; }

        [JsonProperty("price")] public decimal Price { get; set; }

        public Product(int? id = null, string? name = null, string? category = null, string? description = null,
            decimal? price = null)
        {
            Id = id ?? 1;
            Name = name ?? "Default name";
            Category = category ?? "Default category";
            Description = description ?? "Default description";
            Price = price ?? 0;
        }

        public override string ToString() =>
            $"ID: {Id}\nName: {Name}\nCategory: {Category}\nDescription: {Description}\nPrice: {Price} UAH";
    }
}
