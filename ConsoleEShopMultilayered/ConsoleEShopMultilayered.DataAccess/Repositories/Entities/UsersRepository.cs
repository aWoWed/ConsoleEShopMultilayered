﻿using System.Collections.Generic;
using System.Linq;
using ConsoleEShopMultilayered.BusinessLogic;
using ConsoleEShopMultilayered.BusinessLogic.Services.Abstract;
using ConsoleEShopMultilayered.BusinessLogic.Services.Entities;
using ConsoleEShopMultilayered.DataAccess.Models;
using ConsoleEShopMultilayered.DataAccess.Repositories.Abstract;

namespace ConsoleEShopMultilayered.DataAccess.Repositories.Entities
{
    public class UsersRepository : IUsersRepository
    {
        private readonly IDictionary<int, User> _users;
        private readonly IFileService _fileService;
        private readonly IJsonService _jsonService;

        public UsersRepository(AppContext context, ServiceManager serviceManager)
        {
            _fileService = serviceManager.FileService;
            _jsonService = serviceManager.JsonService;
            _users = context.Users;
        }

        public void Load(string fileName)
        {
            var users = _jsonService.Deserialize<IEnumerable<User>>(_fileService.Read(fileName)) ?? new List<User>();

            foreach (var user in users)
                Save(user);
        }
        public void Write(string fileName) => FileService.Write(fileName, _jsonService.Serialize(_users.Values));

        public IDictionary<int, User> GetAll() => _users;

        public User GetById(int id)
        {
            var user = _users[id];
            if (user == null) throw new KeyNotFoundException("User with such Id does not exist.");
            return user;
        }

        public User GetUserByLogin(string login) => _users.FirstOrDefault(user => user.Value.Login == login).Value;
        public User GetUserByLoginAndPassword(string login, string password) => _users.FirstOrDefault(user => user.Value.Login == login && user.Value.Password == password).Value;

        public void Save(User user, bool isWrite = true)
        {

            if (_users.ContainsKey(user.Id))
            {
                _users[user.Id] = user;
                if (isWrite) Write("users.json");
                return;
            }


            _users.Add(user.Id, user);
            if (isWrite) Write("users.json");
        }

    }
}
